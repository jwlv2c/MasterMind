

def getHint(guess):
    blackPegs = 0
    whitePegs = 0
    while True:
        correct = False
        while not correct:
            blackPegs = int(input("Black Pegs?\n"))
            if blackPegs <= 4 and blackPegs >= 0:
                correct = True
            else:
                print("That is not a valid input")
                continue
        correct = False
        while not correct:
            whitePegs = int(input("White Pegs?\n"))
            if whitePegs <= 4 and whitePegs >= 0:
                correct = True
            else:
                print("That is not a valid input")
                continue
        if (blackPegs+whitePegs) > 4:
            print("Too many pegs!")
            continue
        else:
            break
    return {'color': whitePegs,'position': blackPegs}


