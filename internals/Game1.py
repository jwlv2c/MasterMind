from functools import reduce
from internals import Hinter
from internals import guesser
from internals import listCodes
from internals import PlayerHint
from internals import funs
from internals import GPIOcall

class Game1():
    """
    A Game object wraps the game logic and user interface.
    Currently, the number of symbols is hardcoded to 6 and the code length
    is hardcoded to 4. Also, the only game mode is with a human secret keeper
    and computer guesser.
    """
    """
    a : black
    b : white
    c : red
    d : blue
    e : yellow
    f : green
    g : orange
    h : brown
    """
    symbols = 'abcdef' #'abcdef' is default (abc is Octoboard 1, def is Octoboard 2)
    secretlength = 4

    def __init__(self):
        self.allcodes = listCodes.listCodes(self.symbols, self.secretlength)
        self.guesser = guesser.KnuthGuesser(self.allcodes)
        print("======================RESET======================")
        self.guessSecret()

    def guessSecret(self):
        print("\nAttempting to break your code!")
        hint = None
        for i in range(10):
            guess = self.guesser.getGuess(hint)
            print("\nGuess #{}: {}".format(i+1, funs.str2Color(guess)))
            GPIOcall.raspPiGPIOCall(funs.str2Color(guess))
            hint = PlayerHint.getHint(guess)
            print("Hint: {} correct colors, ".format(hint['color']),
                "{} correct positions\n".format(hint['position']))
            if hint['position'] == self.secretlength:
                print("ALGORITHM COMPLETE \nROBOT WON!")
                victory = "Victory"
                GPIOcall.raspPiGPIOCall(victory)
                return
        print("Oh no! You beat me!")
