def credits():
    print("Project Name: Mastermind\n")
    print('Developers: \n \t Python Programmer: \t Jacob "Starkey" Listhartke')
    print('\t Equipment Fabricator: \t Jennifer Keeney')
    print("\t Robot Programmer: \t Connor O'Leary\n")

    print("Special thanks to:")
    print("\t Missouri S&T Mechanical and Aerospace Engineering Department")
    print("\t Dr. Douglas Bristow for being an awesome professor and always willing to help")
    print("\t Lee Ma for being a patient Lab Assistant")
    print("\t That one company that donated the FANUC LR Mate 200i robot to the MAE Department")
    print("\t John Furby, Diego Garcia, and Tyler Thompson for the inspiration via their project in 2011")
    print("\t Stuart Miller who helped us debug our Algorithm so that it would work")
    print("\t The Raspberry Pi Foundation for creating these awesome mini-computers")

    print("Uber extra thanks to:")
    print("\t jonboiser: Who provided an open Github for a Python 3 Mastermind Solving Algorithm")
    print("\t\t (Which this set of code heavily relies on)\n\n")
