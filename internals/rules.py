def rules():
    print("\nThe goal of the game is to guess a hidden color code created by the codemaster")
    print("There are a total of 6 colors to choose from:\n\t red\t blue\n\t yellow\t green\n\t black\t white")
    print("The code consists of 4 colors, chosen in any order by the codemaster\n")
    print("The codebreaker has 10 tries to break the code using the following rules:\n")
    print("\t Black Peg: correct color is in the correct position\n")
    print("\t White Peg: correct color is in the wrong position\n")
    print("\t No Peg: incorrect color\n")