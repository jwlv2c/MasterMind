import random
from internals import GPIOcall

class Game2():
    def __init__(self):
        print("\n\n\tGenerating Combination....")
        colors = ["red", "blue", "yellow", "green", "black", "white"]
        combo = self.getcombination(colors)
        #print(combo)
        print("\tGot it! Make your guess!\n")

        for i in range(9):
            print("Guess #{}:".format(i+1))
            firstguess = self.getGuess(1)
            secondguess = self.getGuess(2)
            thirdguess = self.getGuess(3)
            fourthguess = self.getGuess(4)
            guess = [firstguess, secondguess, thirdguess, fourthguess]
            whitepeg = 0
            blackpeg = 0
            index = 0
            pegcall = ""
            for j in guess:
                print(j)
                if j == combo[0]:
                    if index == 0:
                        blackpeg += 1
                        pegcall += "Black  "
                    else:
                        whitepeg +=1
                        pegcall += "White  "
                elif j == combo[1]:
                    if index == 1:
                        blackpeg += 1
                        pegcall += "Black  "
                    else:
                        whitepeg +=1
                        pegcall += "White  "
                elif j == combo[2]:
                    if index == 2:
                        blackpeg += 1
                        pegcall += "Black  "
                    else:
                        whitepeg +=1
                        pegcall += "White  "
                elif j == combo[3]:
                    if index == 3:
                        blackpeg += 1
                        pegcall += "Black  "
                    else:
                        whitepeg +=1
                        pegcall += "White  "
                index += 1
                print(pegcall)
            if whitepeg == 1 and blackpeg == 3:
                whitepeg = 0
                blackpeg = 4
                pegcall = "Black  " + "Black  " + "Black  " + "Black  "
            GPIOcall.raspPiGPIOCall(pegcall)
            print("{} white pegs, ".format(int(whitepeg)),
                  "{} black pegs\n".format(int(blackpeg)))
            if blackpeg == 4:
                print("You Won!")
                victory = "Victory"
                GPIOcall.raspPiGPIOCall(victory)
                break
        print("You lost!\n\n Better luck next time!")

    def getGuess(self,i):
        correctguess = False
        guess = ""
        while not correctguess:
            guess = str(input("\tEnter color #{} of your choice:\n\t\t".format(i)))
            if guess != "Red" and guess != "red" \
                    and guess != "Blue" and guess != "blue" \
                    and guess != "Yellow" and guess != "yellow" \
                    and guess != "Green" and guess != "green" \
                    and guess != "Black" and guess != "black" \
                    and guess != "White" and guess != "white":
                print("That is not a valid option!")
                continue

            if guess == "red":
                guess = "Red"
            elif guess == "blue":
                guess = "Blue"
            elif guess == "yellow":
                guess = "Yellow"
            elif guess == "green":
                guess = 'Green'
            elif guess == "black":
                guess = "Black"
            elif guess == "white":
                guess = "White"
            elif guess == "orange":
                guess = "Orange"
            elif guess == "brown":
                guess = "Brown"
            correctguess = True
        return guess

    def getcombination(self,colors):
        colorindex1 = random.randint(1, len(colors))-1
        colorindex2 = random.randint(1, len(colors))-1
        colorindex3 = random.randint(1, len(colors))-1
        colorindex4 = random.randint(1, len(colors))-1
        print(colorindex1,colorindex2,colorindex3,colorindex4)
        colordiff = False
        '''
        while not colordiff:
            if colorindex2 == colorindex1:
                colorindex2 = random.randint(1, len(colors))-1
                continue
            if colorindex3 == colorindex2 or colorindex3 == colorindex1:
                colorindex3 = random.randint(1, len(colors))-1
                continue
            if colorindex4 == colorindex1 or colorindex4 == colorindex2 or colorindex4 == colorindex3:
                colorindex4 = random.randint(1, len(colors))-1
                continue
            colordiff = True
        '''
        color1 = str(colors[colorindex1])
        color2 = str(colors[colorindex2])
        color3 = str(colors[colorindex3])
        color4 = str(colors[colorindex4])
        combo = [color1,color2,color3,color4]
        return combo
