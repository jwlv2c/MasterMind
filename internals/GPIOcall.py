
import RPi.GPIO as GPIO
import time

class ColorCall():
    def __init__(self, GPIOPin):
        self.pin = GPIOPin
        GPIO.setup(self.pin, GPIO.OUT)
        self.turnOffQuick()

    def turnOn(self):
        GPIO.output(self.pin, False)

    def turnOffQuick(self):
        GPIO.output(self.pin, True)
        time.sleep(0.1)

    def turnOff(self):
        GPIO.output(self.pin, True)
        time.sleep(1)

class ActionCompleted():
    def __init__(self,GPIOPin):
        self.pin = GPIOPin
        #GPIO.setup(self.pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

    def waitinginput(self):
        input('\nPress Enter when Robot is done Moving\t')
       
        '''
        while GPIO.input(self.pin) == GPIO.LOW:
            time.sleep(0.2)
        '''

def raspPiGPIOCall(guess):
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BOARD)
    Acknowledge = ActionCompleted(40)
    victory = ColorCall(38)
    black = ColorCall(37)
    white = ColorCall(36)
    red = ColorCall(35)
    blue = ColorCall(33)
    yellow = ColorCall(32)
    green = ColorCall(31)

    colorcalled = ""
    index = 0
    for i in guess:
        colorcalled = colorcalled + str(i)
        if float(index) == 6 or float(index) == 13 or float(index) == 20 or float(index) == 27 or float(index) == 34:
            #print(colorcalled)
            if colorcalled == "Black  ":
                black.turnOn()
                Acknowledge.waitinginput()
                black.turnOff()
            elif colorcalled == "White  ":
                white.turnOn()
                Acknowledge.waitinginput()
                white.turnOff()
            elif colorcalled == "Victory":
                victory.turnOn()
                Acknowledge.waitinginput()
                victory.turnOff()
            elif colorcalled == "Red    ":
                red.turnOn()
                Acknowledge.waitinginput()
                red.turnOff()
            elif colorcalled == "Blue   ":
                blue.turnOn()
                Acknowledge.waitinginput()
                blue.turnOff()
            elif colorcalled == "Yellow ":
                yellow.turnOn()
                Acknowledge.waitinginput()
                yellow.turnOff()
            elif colorcalled == "Green  ":
                green.turnOn()
                Acknowledge.waitinginput()
                green.turnOff()
            else:
                print("Something Broke!")
                print("*****ERROR ERROR*****")
                input("Press Enter to Continue...")
            colorcalled = ""
        index += 1
    GPIO.cleanup()
    print("===== ACTION COMPLETE =====")
