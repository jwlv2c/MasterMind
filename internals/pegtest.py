from internals import GPIOcall

def Pegtest():
	mainmenu = ["\t0: Return to Previous Menu","\t1: Victory","\t2: Black Peg" ,"\t3: White Peg", "\t4: Red Peg","\t5: Blue Peg","\t6: Green Peg","\t7: Yellow Peg"]
	running = True

	while running:
		print("\nPEG TEST MENU:")
		for item in mainmenu:
			print(item)
		choice = input("\nSelect an option: \n")
		print("You chose:", choice)
		index = int(choice)-1

		if index == -1: #User quits the program
			running = False
		elif index == 1: #User calls Black Peg
			string = "Black  "
			GPIOcall.raspPiGPIOCall(string)
			continue
		elif index == 2: #User calls White Peg
			string = "White  "
			GPIOcall.raspPiGPIOCall(string)
			continue
		elif index == 3: #User calls Red Peg
			string = "Red    "
			GPIOcall.raspPiGPIOCall(string)
			continue
		elif index == 4: #User calls Blue Peg
			string = "Blue   "
			GPIOcall.raspPiGPIOCall(string)
			continue
		elif index == 5: #User calls Green Peg
			string = "Green  "  
			GPIOcall.raspPiGPIOCall(string)
			continue
		elif index == 6: #User calls Yellow Peg
			string = "Yellow "
			GPIOcall.raspPiGPIOCall(string)
			continue	
		elif index == 0: #User calls victory action
			string = "Victory"
			GPIOcall.raspPiGPIOCall(string)
			continue	
		else:
			print("That is not a valid option")
