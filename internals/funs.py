def let2Color(letter):
    if letter == 'a':
        return "Black  "
    elif letter == 'b':
        return "White  "
    elif letter == 'c':
        return "Red    "
    elif letter == 'd':
        return "Blue   "
    elif letter == 'e':
        return "Yellow "
    elif letter == 'f':
        return "Green  "
    elif letter == 'g':
        return "Orange "
    elif letter == 'h':
        return "Brown  "
    else:
        print("That is not a valid symbol")


def str2Color(longstring):
    emptystring = ""
    for i in longstring:
        if i == 'a':
            emptystring += "Black  "
        elif i == 'b':
            emptystring += "White  "
        elif i == 'c':
            emptystring += "Red    "
        elif i == 'd':
            emptystring += "Blue   "
        elif i == 'e':
            emptystring += "Yellow "
        elif i == 'f':
            emptystring += "Green  "
        elif i == 'g':
            emptystring += "Orange "
        elif i == 'h':
            emptystring += "Brown  "
        else:
            print("That is not a valid symbol")
    return emptystring