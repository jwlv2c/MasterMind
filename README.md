This Git was created to help with the Group #4 Robotics Project for ME 5449 at
 Missouri S&T. 

The Goal: Create a Python script to play the game mastermind, and interface it
 with the Robot in the Lab using the I/O's of the robot

Project Members:
Jacob "Starkey" Listhartke
Connor O'leary
Jennifer Keeney

PIN ASSIGNMENTS:
Acknowledge: Pin 40
    victory: Pin 38
      black: Pin 37
      white: Pin 36
        red: Pin 35
       blue: Pin 34
     yellow: Pin 33
      green: Pin 32
