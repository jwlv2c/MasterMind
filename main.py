'''
==========================================================================
  Name: Jacob Listhartke              Date started: 11/03/2017
  Program: Mastermind.py              Class: ME 5449: Robotic Manipulators
  Description: Getting the computer to play MASTERMIND
==========================================================================
'''
from internals import Localcredits
from internals import Game1
from internals import rules
from internals import Game2
from internals import pegtest

print('======================================== PROGRAM START ========================================\n')

#MAIN MENU
mainmenu = ["\t0: Quit","\t1: How to Play" ,"\t2: Robot is Player", "\t3: Robot is Master","\t4: Credits","\t5: Pegtest"]
running = True

print("WELCOME TO MASTERMIND")

while running:
    print("\nMAIN MENU:")
    for item in mainmenu:
        print(item)
    choice = input("\nSelect an option: \n")
    print("You chose:", choice)
    index = int(choice)-1

    if index == -1: #User quits the program
        running = False
    elif index == 0: #User asks for the rules of the game
        rules.rules()
        input("Press Enter to continue...\t")
        continue
    elif index == 1: #User wants to make the Robot the code breaker
        g = Game1.Game1()
        continue
    elif index == 2: #User wants the Robot to create the code and judge
        g = Game2.Game2()
        continue
    elif index == 3: #User wants credits to roll
        Localcredits.credits()
        input("Press Enter to continue...\t")
        continue
    elif index == 4: #Pegtest
        pegtest.Pegtest()
        continue
    else:
        print("That is not a valid option")
print("Thank you for playing!")

print('======================================== PROGRAM END ========================================')
